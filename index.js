console.log("hello word")

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function addusers(trainer){
	users[users.length]= trainer;
}
addusers("Jovin");

console.log(users)

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/


function finditem(index){
	return users[index]
}
let itemfound = finditem(0);
console.log(itemfound);




/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/
users.length = users.length-1

function addusers1(trainer1){
	users[users.length]= trainer1;
}
addusers1("Jovin");


function finditem(index){
	return users[index]
}
let itemfound1 = finditem(4);
console.log(itemfound1);


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/
function user1(vin){
	let users2 = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
	console.log(users2);
	users2[3] = "jovin"
	console.log(users2)
}
user1()



/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/
function user3(vin1){
	let users4 = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
	users4.length =users4.length-4
	console.log(users4)
}
user3()



/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

//let isUserempty = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
/*
function user6([va1,var2,var3,var4]){
	if(user6[index] === 0){
		console.log(false)
	}else{
		console.log(true);
	}
}
let isUserempty = user6(["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"]);
users6()
*/

let isUserempty = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

for(let index = 0; index < isUserempty.length; index++){
	if(isUserempty[index] === 0){
		console.log(false)
	}else{
		console.log(true)
	}
}